import React from "react"
import user from "../images/user.png"

const ContactDetail = (props) => {

    const { id, name, email } = props.location.state.contact;
    return (
        <div className="ui card">
            <div className="image">
                <img src={user} alt="user" />
            </div>
            <div className="content">
                <div className="header">
                    {name}

                </div>
                <div className="meta">
                    <span className="date">
                       Id: {id}
                    </span>
                </div>
                <div className="description">
                    {email}
                </div>
            </div>
            <div className="header"></div>
        </div>
    )
};

export default ContactDetail