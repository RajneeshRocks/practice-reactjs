import React from "react"
import { Link } from "react-router-dom"
import user from "../images/user.png"

const ContactCard = (props) => {

    const { id, name, email } = props.contact;
    return (
        <div className="item">
            <img className="ui avatar image" src={user} alt="user" />
            <div className="content">
                <div className="header">{name}</div>
                <div>{email}</div>
            </div>
            <span><i className="trash alternate outline icon" onClick={() => props.clickHandler(id)}></i></span>
            <span><Link to={{ pathname: `Contact-Detail/${id}`, state: { contact: props.contact } }}><i className="info icon"></i></Link></span>

        </div>
    );
}
export default ContactCard;