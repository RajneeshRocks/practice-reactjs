
import React, { useEffect, useState } from "react"
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom"
import { uuid } from 'uuidv4'
import './App.css';
import Header from "./Header"
import AddContact from "./AddContact"
import ContactList from "./ContactList"
import ContactDetail from "./ContactDetail";

function App() {
  const LOCAL_STORAGE_KEY = "contacts";
  const [contacts, setContacts] = useState([]);

  const addContactHandler = (contact) => {
    console.log(contact);
    setContacts([...contacts, { id: uuid(), ...contact }]);
  }

  const removeContactHandler = (id) => {
    const newContact = contacts.filter(cont => { return cont.id !== id });
    setContacts(newContact);
  }

  useEffect(() => {
    const retriveContacts = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY));
    if (retriveContacts) setContacts(retriveContacts);
  }, []);

  useEffect(() => {
    localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(contacts));
  }, [contacts]);

  const ComponentRouting = () => {
    return (
      <Switch>
        <Route path="/"
          exact
          render={(props) => (
            <ContactList
              {...props}
              contacts={contacts}
              getContactId={removeContactHandler}
            />
          )}
        />
        <Route path="/Contact-Detail/:id" component={ContactDetail} />
        <Route path="/add" component={() => <AddContact addContactHandler={addContactHandler} />} />
      </Switch>
    )
  };

  return (
    <div className="ui container">
      <Router>
        <Header />
        <div className="main">
          <div className="ui grid ">
            <div className="four wide column">
              <div className="ui vertical fluid tabular menu">

                <Link to="/" className="item active">Contacts</Link>
                <Link to="/add" className="item">Add Contact</Link>
              </div>
            </div>
            <div className="twelve wide stretched column">
              <div className="">

                <ComponentRouting />
              </div>
            </div>
          </div>

        </div>
        {/* <AddContact addContactHandler={addContactHandler} />
        <ContactList contacts={contacts} getContactId={removeContactHandler} /> */}
      </Router>

    </div>

  );
}

export default App;
